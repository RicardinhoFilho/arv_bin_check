#include <stdlib.h>
#include <stdio.h>

typedef struct node node;
struct node
{
    int num;
    node *left;
    node *right;
};

node *cria_node(int num)
{
    node *root = (node *)malloc(sizeof(node));
    root->num = num;
    root->left = NULL;
    root->right = NULL;
    if (num > root->num)
        root->right->num = num;
    if (num < root->num)
        root->left->num = num;
    return root;
}

void print(node *root)
{
    if (root != NULL)
    {
        printf("%d\t", root->num);
        print(root->left);
        print(root->right);
    }
}

void free_tree(node *root)
{
    if (root != NULL)
    {
        free_tree(root->left);
        free_tree(root->right);
        free(root);
    }
}

int arv_bin_check(node *root)
{
    if (root != NULL)
    {
        if (root->right == NULL && root->left == NULL)
        {
            return 1;
        }
        

        if (root->right!=NULL && root->right->num < root->num)
        {
            return 0;
        }
        if (root->left!=NULL  && root->left->num > root->num)
        {
            return 0;
        }
    printf("%d\n\n",arv_bin_check(root->left) );
        if (arv_bin_check(root->left) == 0 || arv_bin_check(root->right) == 0)
        {
            
            return 0;
        }
    }

    return 1;
}

void main()
{
    node *root = cria_node(2);
    root->left = cria_node(1);
    root->right = cria_node(3);
    //root->left->left = cria_node(10);
    root->right->right = cria_node(6);

 if(arv_bin_check(root)==0){
     printf("ÁRVORE NÃO É DE BUSCA\n");
 }else{
     printf("ÁRVORE  É DE BUSCA\n");
 }

    print(root);
    printf("\n");
    free_tree(root);
}
